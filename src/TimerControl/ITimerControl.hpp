#ifndef __I_TIMER_CONTROL_HPP__
#define __I_TIMER_CONTROL_HPP__

#include <stdint.h>

class ITimerControl
{
public:
    virtual void StartPwm(uint16_t frequency, double dutyPercent) = 0;
    virtual void GeneratePulse(uint16_t lengthMiliseconds) = 0;
    virtual uint32_t GetPeriodNs() = 0;
    virtual void Stop() = 0;
    virtual ~ITimerControl() = default;
};

#endif
