#include "MemoryAccess.hpp"
#include <stdexcept>

MemoryAccess::MemoryAccess(uint32_t* reference, uint32_t length) :
    _reference{ reference }, _length{ length }
{
}

uint32_t MemoryAccess::ReadRegister(uint32_t offset)
{
    CheckOffset(offset);
    offset /= 4;
    return *(_reference + offset);
}

void MemoryAccess::WriteRegister(uint32_t offset, uint32_t data)
{
    CheckOffset(offset);
    offset /= 4;
    *(_reference + offset) = data;
}

void MemoryAccess::CheckOffset(uint32_t offset)
{
    if (offset >= _length)
    {
        throw std::logic_error("Access out of memory.");
    }
}