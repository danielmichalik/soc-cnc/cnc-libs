#ifndef __MEMORY_ACCESS_HPP__
#define __MEMORY_ACCESS_HPP__

#include <stdint.h>
#include "IMemoryAccess.hpp"

class MemoryAccess : public IMemoryAccess
{
public:
    explicit MemoryAccess(uint32_t* reference, uint32_t length);
    uint32_t ReadRegister(uint32_t offset) override;
    void WriteRegister(uint32_t offset, uint32_t data) override;
private:
    void CheckOffset(uint32_t offset);
    uint32_t * _reference;
    uint32_t _length;
};

#endif