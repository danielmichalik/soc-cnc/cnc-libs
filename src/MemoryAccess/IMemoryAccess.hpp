#ifndef __I_MEMORY_ACCESS_HPP__
#define __I_MEMORY_ACCESS_HPP__

class IMemoryAccess
{
public:
    virtual uint32_t ReadRegister(uint32_t offset) = 0;
    virtual void WriteRegister(uint32_t offset, uint32_t data) = 0;
    virtual ~IMemoryAccess() = default;
};
#endif