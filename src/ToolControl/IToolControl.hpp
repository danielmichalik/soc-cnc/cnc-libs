#ifndef __I_TOOL_CONTROL_HPP__
#define __I_TOOL_CONTROL_HPP__

#include <stdint.h>

enum ToolControlMode
{
    Spindle,
    Dispenser,
};

class IToolControl
{
public:
    virtual void SetMode(ToolControlMode mode) = 0;
    virtual ToolControlMode GetMode() = 0;
    virtual void StartSpindle(uint16_t rpm) = 0;
    virtual void StopSpindle() = 0;
    virtual void Dispense(uint16_t timeMiliseconds) = 0;
    virtual ~IToolControl() = default;
};
#endif