#include "ToolControl.hpp"
#include "ConfigFile.hpp"
#include <stdexcept>

ToolControl::ToolControl(std::unique_ptr<ITimerControl> timerControl, std::unique_ptr<IPid> pid) :
    _timerControl{ std::move(timerControl) }, _pid{ std::move(pid) }
{
    _mode = ToolControlMode::Spindle;
    _spindleIsRunning = false;
    _rpmSetPoint = 0;
}

void ToolControl::SetMode(ToolControlMode mode)
{
    if ((mode != ToolControlMode::Spindle) && _spindleIsRunning)
    {
        throw std::logic_error("Spindle is running!");
    }

    _mode = mode;
}

ToolControlMode ToolControl::GetMode()
{
    return _mode;
}

void ToolControl::StartSpindle(uint16_t rpm)
{
    _rpmSetPoint = rpm;
    double percentage = RpmToPercentage(rpm);
    _timerControl->StartPwm(CONFIG_PWM_FREQUENCY, percentage);
    _spindleIsRunning = true;
}

void ToolControl::StopSpindle()
{
    _rpmSetPoint = 0;
    _timerControl->StartPwm(CONFIG_PWM_FREQUENCY, CONFIG_DUTY_MIN);
    _spindleIsRunning = false;
}

void ToolControl::Dispense(uint16_t timeMiliseconds)
{
    _timerControl->GeneratePulse(timeMiliseconds);
}

void ToolControl::Update()
{
    if((_mode == ToolControlMode::Spindle) && (_spindleIsRunning))
    {
        auto period = _timerControl->GetPeriodNs();
        auto actualRpm = 1e9 / period;
        auto pidResult = _pid->Calculate(_rpmSetPoint, actualRpm);
        double percentage = RpmToPercentage(pidResult);
        _timerControl->StartPwm(CONFIG_PWM_FREQUENCY, percentage);
    }
}

double ToolControl::RpmToPercentage(uint16_t rpm)
{
    return (CONFIG_DUTY_MIN + (CONFIG_DUTY_RANGE * (rpm / CONFIG_RPM_MAX)));
}