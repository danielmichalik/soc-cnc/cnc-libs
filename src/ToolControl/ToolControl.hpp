#ifndef __TOOL_CONTROL_HPP__
#define __TOOL_CONTROL_HPP__

#include "IToolControl.hpp"
#include "ITimerControl.hpp"
#include "IPid.hpp"
#include <stdint.h>
#include <memory>

class ToolControl : public IToolControl
{
public:
    explicit ToolControl(std::unique_ptr<ITimerControl> timerControl, std::unique_ptr<IPid> pid);
    void SetMode(ToolControlMode mode) override;
    ToolControlMode GetMode() override;
    void StartSpindle(uint16_t rpm) override;
    void StopSpindle() override;
    void Dispense(uint16_t timeMiliseconds) override;
    void Update();
private:
    double RpmToPercentage(uint16_t rpm);
    std::unique_ptr<ITimerControl> _timerControl;
    std::unique_ptr<IPid> _pid;
    ToolControlMode _mode;
    bool _spindleIsRunning;
    uint16_t _rpmSetPoint;
};
#endif