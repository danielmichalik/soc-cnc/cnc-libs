#ifndef __CONFIG_FILE_HPP__
#define __CONFIG_FILE_HPP__


// Tool control
#define CONFIG_RPM_MAX                  (double)    20400   // rer/min
#define CONFIG_RPM_DEFAULT              (double)    3000    // rer/min
#define CONFIG_DUTY_MIN                 (int)       5       // %
#define CONFIG_DUTY_MAX                 (int)       10      // %
#define CONFIG_DUTY_RANGE               (CONFIG_DUTY_MAX - CONFIG_DUTY_MIN)
#define CONFIG_PWM_FREQUENCY            (int)       50      // Hz

// Movement control
#define CONFIG_SPEED_CLOCK_FREQUENCY    (int)       (1e6)   // Hz
#define CONFIG_X_PULSES_PER_MM          (double)    ((200 / 1.25) * 4)  // 1.25 mm/rev; 200 st/rev; /4 microsteping
#define CONFIG_Y_PULSES_PER_MM          (double)    ((200 / 1.25) * 4)  // 1.25 mm/rev; 200 st/rev; /4 microsteping
#define CONFIG_Z_PULSES_PER_MM          (double)    ((200 / 1.25) * 4)  // 1.25 mm/rev; 200 st/rev; /4 microsteping

#endif