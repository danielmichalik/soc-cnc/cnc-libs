#ifndef __I_STEPPER_MOTOR_HPP__
#define __I_STEPPER_MOTOR_HPP__

#include "StepperMode.hpp"

class IStepperMotor
{
public:
    virtual void Enable() = 0;
    virtual void Disable() = 0;

    virtual void SetMode(StepperMode mode) = 0;
    virtual StepperMode GetMode() = 0;

    virtual uint32_t GetRequiredPosition() = 0;
    virtual uint32_t GetActualPosition() = 0;
    virtual void SetPosition(uint32_t position) = 0;
    
    virtual uint32_t GetSpeedPeriod() = 0;
    virtual void SetSpeedPeriod(uint32_t period) = 0;
    
    virtual void GenerateSyncPulse() = 0;
    virtual void SyncOn() = 0;
    virtual void SyncOff() = 0;

    virtual void ResetCounter() = 0;
    
    virtual ~IStepperMotor() = default;
};
#endif