#ifndef __STEPPER_MOTOR_HPP__
#define __STEPPER_MOTOR_HPP__

#include <stdint.h>
#include <memory>
#include "IStepperMotor.hpp"
#include "IMemoryAccess.hpp"

enum RegisterOffsets
{
    RequiredPosition = 0,
    ActualPosition = 4,
    Speed = 8,
    Acceleration = 12,
    ModeOfControl = 16,
    Commands = 20,
    Status = 24
};

enum CommandOffsets
{
    CmdEnable = 0,
    CmdSync = 1,
    CmdResetCounter = 2
};

class StepperMotor : public IStepperMotor
{
public:
    explicit StepperMotor(std::unique_ptr<IMemoryAccess> memoryAccess);
    void Enable() override;
    void Disable() override;

    void SetMode(StepperMode mode) override;
    StepperMode GetMode() override;

    uint32_t GetRequiredPosition() override;
    uint32_t GetActualPosition() override;
    void SetPosition(uint32_t position) override;

    uint32_t GetSpeedPeriod() override;
    void SetSpeedPeriod(uint32_t period) override;

    void GenerateSyncPulse() override;
    void SyncOn() override;
    void SyncOff() override;

    void ResetCounter() override;
private:
    void SetBit(uint32_t* reg, uint8_t offset);
    void ResetBit(uint32_t* reg, uint8_t offset);
    std::unique_ptr<IMemoryAccess> _memoryAccess;
};

#endif