#ifndef __I_STEPPER_MODE_HPP__
#define __I_STEPPER_MODE_HPP__

enum StepperMode
{
    POSITION_CONTROL = 0,
    SPEED_CONTROL = 1,
    CALIB = 2
};

#endif