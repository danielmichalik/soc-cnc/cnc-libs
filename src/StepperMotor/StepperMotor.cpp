#include "StepperMotor.hpp"
#include "StepperMode.hpp"

StepperMotor::StepperMotor(std::unique_ptr<IMemoryAccess> memoryAccess) :
    _memoryAccess{ std::move(memoryAccess) }
{}

void StepperMotor::Enable()
{
    auto value = _memoryAccess->ReadRegister(RegisterOffsets::Commands);
    SetBit(&value, CommandOffsets::CmdEnable);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
}

void StepperMotor::Disable()
{
    auto value = _memoryAccess->ReadRegister(RegisterOffsets::Commands);
    ResetBit(&value, CommandOffsets::CmdEnable);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
}

void StepperMotor::SetMode(StepperMode mode)
{
    _memoryAccess->WriteRegister(RegisterOffsets::ModeOfControl, (uint32_t)mode);
}

StepperMode StepperMotor::GetMode()
{
    return (StepperMode)_memoryAccess->ReadRegister(RegisterOffsets::ModeOfControl);
}

uint32_t StepperMotor::GetRequiredPosition()
{
    return _memoryAccess->ReadRegister(RegisterOffsets::RequiredPosition);
}

uint32_t StepperMotor::GetActualPosition()
{
    return _memoryAccess->ReadRegister(RegisterOffsets::ActualPosition);
}

void StepperMotor::SetPosition(uint32_t position)
{
    _memoryAccess->WriteRegister(RegisterOffsets::RequiredPosition, position);
}

uint32_t StepperMotor::GetSpeedPeriod()
{
    return _memoryAccess->ReadRegister(RegisterOffsets::Speed);
}

void StepperMotor::SetSpeedPeriod(uint32_t period)
{
    _memoryAccess->WriteRegister(RegisterOffsets::Speed, period);
}

void StepperMotor::GenerateSyncPulse()
{
    SyncOn();
    SyncOff();
}

void StepperMotor::SyncOn()
{
    auto value = _memoryAccess->ReadRegister(RegisterOffsets::Commands);
    SetBit(&value, CmdSync);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
}

void StepperMotor::SyncOff()
{
    auto value = _memoryAccess->ReadRegister(RegisterOffsets::Commands);
    ResetBit(&value, CmdSync);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
}

void StepperMotor::ResetCounter()
{
    auto value = _memoryAccess->ReadRegister(RegisterOffsets::Commands);
    SetBit(&value, CmdSync);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
    ResetBit(&value, CmdSync);
    _memoryAccess->WriteRegister(RegisterOffsets::Commands, value);
}

void StepperMotor::SetBit(uint32_t* reg, uint8_t offset)
{
    *(reg) |= (1 << offset);
}

void StepperMotor::ResetBit(uint32_t* reg, uint8_t offset)
{
    *(reg) &= ~(1 << offset);
}