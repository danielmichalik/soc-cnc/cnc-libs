#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <string>
#include <vector>

class Utils
{
public:
    static bool StringContains(std::string input, std::string phrase);
    static bool GetGroupsByRegex(const char* regEx, std::string &inputString, std::vector<std::string> &result);
    static bool GetSingleMatchByRegex(const char* regEx, std::string &inputString, std::string &result);
};

#endif