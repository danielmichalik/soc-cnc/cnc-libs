#include "Utils.hpp"
#include <regex>

bool Utils::StringContains(std::string input, std::string phrase)
{
    return (input.find(phrase) != std::string::npos);
}

bool Utils::GetGroupsByRegex(const char* regEx, std::string &inputString, std::vector<std::string> &result)
{
    std::regex commandPattern(regEx);
    std::smatch sm;

    if (std::regex_search(inputString, sm, commandPattern))
    {
        for (uint32_t i = 0; i < sm.size(); i++)
        {
            result.push_back(sm.str(i));
        }
        return true;
    }

    return false;
}

bool Utils::GetSingleMatchByRegex(const char* regEx, std::string &inputString, std::string &result)
{
    std::regex commandPattern(regEx);
    std::smatch sm;

    if (std::regex_search(inputString, sm, commandPattern))
    {
        result = sm.str(0);
        return true;
    }

    return false;
}