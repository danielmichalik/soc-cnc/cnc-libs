#ifndef __I_MOVEMENT_CONTROL_HPP__
#define __I_MOVEMENT_CONTROL_HPP__

#include "Axis.hpp"

class IMovementControl
{
public:
    virtual void MoveAbsolute(uint32_t x, uint32_t y, uint32_t z, uint32_t speed) = 0;
    virtual void MoveRelative(int32_t dx, int32_t dy, int32_t dz, uint32_t speed) = 0;
    virtual void SetHomePostion(AxisSelector axis) = 0;
    virtual ~IMovementControl() = default;
};
#endif