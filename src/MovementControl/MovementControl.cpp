#include "MovementControl.hpp"
#include "ConfigFile.hpp"
#include <math.h>

MovementControl::MovementControl(
    std::unique_ptr<IStepperMotor> stepperMotorX,
    std::unique_ptr<IStepperMotor> stepperMotorY,
    std::unique_ptr<IStepperMotor> stepperMotorZ):
    _stepperMotorX { std::move(stepperMotorX) },
    _stepperMotorY { std::move(stepperMotorY) },
    _stepperMotorZ { std::move(stepperMotorZ) }
{
    _stepperMotorX->Enable();
    _stepperMotorY->Enable();
    _stepperMotorZ->Enable();
}

void MovementControl::MoveAbsolute(uint32_t x, uint32_t y, uint32_t z, uint32_t speed)
{
    auto actualPositionOfXmm = StepsToMicrometers(_stepperMotorX->GetActualPosition(), CONFIG_X_PULSES_PER_MM);
    auto actualPositionOfYmm = StepsToMicrometers(_stepperMotorY->GetActualPosition(), CONFIG_Y_PULSES_PER_MM);
    auto actualPositionOfZmm = StepsToMicrometers(_stepperMotorZ->GetActualPosition(), CONFIG_Z_PULSES_PER_MM);

    _stepperMotorX->SetPosition(MicrometersToSteps(x, CONFIG_X_PULSES_PER_MM));
    _stepperMotorY->SetPosition(MicrometersToSteps(y, CONFIG_Y_PULSES_PER_MM));
    _stepperMotorZ->SetPosition(MicrometersToSteps(z, CONFIG_Z_PULSES_PER_MM));

    SetSpeed(x - actualPositionOfXmm, y - actualPositionOfYmm, z - actualPositionOfZmm, speed);

    StartPositionMove();
}

void MovementControl::MoveRelative(int32_t dx, int32_t dy, int32_t dz, uint32_t speed)
{
    auto actualPositionOfXmm = StepsToMicrometers(_stepperMotorX->GetActualPosition(), CONFIG_X_PULSES_PER_MM);
    auto actualPositionOfYmm = StepsToMicrometers(_stepperMotorY->GetActualPosition(), CONFIG_Y_PULSES_PER_MM);
    auto actualPositionOfZmm = StepsToMicrometers(_stepperMotorZ->GetActualPosition(), CONFIG_Z_PULSES_PER_MM);

    auto newPositionOfXsteps = MicrometersToSteps(actualPositionOfXmm + dx, CONFIG_X_PULSES_PER_MM);
    auto newPositionOfYsteps = MicrometersToSteps(actualPositionOfYmm + dy, CONFIG_Y_PULSES_PER_MM);
    auto newPositionOfZsteps = MicrometersToSteps(actualPositionOfZmm + dz, CONFIG_Z_PULSES_PER_MM);

    _stepperMotorX->SetPosition(newPositionOfXsteps);
    _stepperMotorY->SetPosition(newPositionOfYsteps);
    _stepperMotorZ->SetPosition(newPositionOfZsteps);

    SetSpeed(dx, dy, dz, speed);

    StartPositionMove();
}

void MovementControl::SetHomePostion(AxisSelector axis)
{
    switch (axis)
    {
        case X:
            _stepperMotorX->ResetCounter();
            break;
        case Y:
            _stepperMotorY->ResetCounter();
            break;
        case Z:
            _stepperMotorZ->ResetCounter();
            break;
        case ALL:
            _stepperMotorX->ResetCounter();
            _stepperMotorY->ResetCounter();
            _stepperMotorZ->ResetCounter();
            break;
        default:
            break;
    }
}

void MovementControl::StartPositionMove()
{
    _stepperMotorX->SetMode(StepperMode::POSITION_CONTROL);
    _stepperMotorY->SetMode(StepperMode::POSITION_CONTROL);
    _stepperMotorZ->SetMode(StepperMode::POSITION_CONTROL);
    _stepperMotorX->GenerateSyncPulse();
}

void MovementControl::SetSpeed(int32_t dx, int32_t dy, int32_t dz, uint32_t speed)
{
    double distance = sqrt((dx*dx) + (dy*dy) + (dz*dz));
    double time = distance / ((double)speed);

    double speedXmmPerseconds = dx / time;
    double speedYmmPerseconds = dy / time;
    double speedZmmPerseconds = dz / time;

    double speedXstepsPerseconds = MicrometersToSteps(speedXmmPerseconds, CONFIG_X_PULSES_PER_MM);
    double speedYstepsPerseconds = MicrometersToSteps(speedYmmPerseconds, CONFIG_Y_PULSES_PER_MM);
    double speedZstepsPerseconds = MicrometersToSteps(speedZmmPerseconds, CONFIG_Z_PULSES_PER_MM);

    double speedPeriodX = (1 / (speedXstepsPerseconds * ((double)speedPeriodUs / CONFIG_SPEED_CLOCK_FREQUENCY)));
    double speedPeriodY = (1 / (speedYstepsPerseconds * ((double)speedPeriodUs / CONFIG_SPEED_CLOCK_FREQUENCY)));
    double speedPeriodZ = (1 / (speedZstepsPerseconds * ((double)speedPeriodUs / CONFIG_SPEED_CLOCK_FREQUENCY)));

    _stepperMotorX->SetSpeedPeriod(speedPeriodX);
    _stepperMotorY->SetSpeedPeriod(speedPeriodY);
    _stepperMotorZ->SetSpeedPeriod(speedPeriodZ);
}

int32_t MovementControl::MicrometersToSteps(int32_t um, double pulsesPerMm)
{
    return (((double)um / 1000) * pulsesPerMm);
}

int32_t MovementControl::StepsToMicrometers(int32_t steps, double pulsesPerMm)
{
    return ((steps * 1000) / pulsesPerMm);
}