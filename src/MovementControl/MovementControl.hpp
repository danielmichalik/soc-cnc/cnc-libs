#ifndef __MOVEMENT_CONTROL_HPP__
#define __MOVEMENT_CONTROL_HPP__

#include <stdint.h>
#include <memory>
#include "IStepperMotor.hpp"
#include "IMovementControl.hpp"

class MovementControl : public IMovementControl
{
public:
    explicit MovementControl(
        std::unique_ptr<IStepperMotor> stepperMotorX,
        std::unique_ptr<IStepperMotor> stepperMotorY,
        std::unique_ptr<IStepperMotor> stepperMotorZ);
    void MoveAbsolute(uint32_t x, uint32_t y, uint32_t z, uint32_t speed) override;
    void MoveRelative(int32_t dx, int32_t dy, int32_t dz, uint32_t speed) override;
    void SetHomePostion(AxisSelector axis) override;
private:
    void StartPositionMove();
    void SetSpeed(int32_t dx, int32_t dy, int32_t dz, uint32_t speed);
    int32_t MicrometersToSteps(int32_t um, double pulsesPerRev);
    int32_t StepsToMicrometers(int32_t steps, double pulsesPerRev);
    std::unique_ptr<IStepperMotor> _stepperMotorX;
    std::unique_ptr<IStepperMotor> _stepperMotorY;
    std::unique_ptr<IStepperMotor> _stepperMotorZ;
    const uint32_t speedPeriodUs = 10;
};

#endif