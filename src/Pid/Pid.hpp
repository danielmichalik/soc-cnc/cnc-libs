#ifndef __PID_HPP__
#define __PID_HPP__

#include "IPid.hpp"
#include <stdint.h>

class Pid : public IPid
{
public:
    explicit Pid(double pConst, double iConst, double dConst, double dtConst, double outputMin, double outputMax);
    double Calculate(double setValue, double actualValue) override;
private:
    double _pConst;
    double _iConst;
    double _dConst;
    double _dtConst;
    double _outputMin;
    double _outputMax;

    double _integralSuma;
    double _preError;
};

#endif