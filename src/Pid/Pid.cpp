#include "Pid.hpp"

Pid::Pid(double pConst, double iConst, double dConst, double dtConst, double outputMin, double outputMax) :
    _pConst{ pConst }, _iConst{ iConst }, _dConst{ dConst }, _dtConst{ dtConst }, _outputMin{ outputMin }, _outputMax{ outputMax }
{}

double Pid::Calculate(double setValue, double actualValue)
{
    double error = setValue - actualValue;

    double p = _pConst * error;

    _integralSuma += error * _dtConst;
    double i = _iConst * _integralSuma;

    double deriv = (error - _preError) / _dtConst;
    double d = _dConst * deriv;

    double result = p + i + d;

    if (result > _outputMax)
    {
        result = _outputMax;
    }
    if (result < _outputMin)
    {
        result = _outputMin;
    }

    _preError = error;

    return result;
}