#ifndef __I_PID_HPP__
#define __I_PID_HPP__

class IPid
{
public:
    virtual double Calculate(double setValue, double actualValue) = 0;
    virtual ~IPid() = default;
};

#endif