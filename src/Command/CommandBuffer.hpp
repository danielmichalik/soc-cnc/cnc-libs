#ifndef __COMMAND_BUFFER_HPP__
#define __COMMAND_BUFFER_HPP__

#include "Command.hpp"
#include <queue>

class CommandBuffer
{
public:
    explicit CommandBuffer(uint16_t capacity);
    void Push(Command cmd);
    Command Pop();
    bool IsEmpty();
    bool IsFull();
private:
    std::queue<Command> _buffer;
    uint16_t _capacity;
};

#endif
