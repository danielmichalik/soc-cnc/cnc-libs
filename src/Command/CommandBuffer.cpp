#include "CommandBuffer.hpp"

CommandBuffer::CommandBuffer(uint16_t capacity) :
    _capacity{ capacity }
{}

void CommandBuffer::Push(Command cmd)
{
    if (IsFull())
    {
        throw std::logic_error("Command buffer is full!");
    }

    _buffer.push(cmd);
}

Command CommandBuffer::Pop()
{
    if(IsEmpty())
    {
        throw std::logic_error("Command buffer is empty!");
    }

    auto result = _buffer.front();
    _buffer.pop();
    return result;
}

bool CommandBuffer::IsEmpty()
{
    return _buffer.empty();
}

bool CommandBuffer::IsFull()
{
    return (_buffer.size() >= _capacity);
}