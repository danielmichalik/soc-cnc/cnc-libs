#ifndef __COMMAND_HPP__
#define __COMMAND_HPP__

#include "Argument.hpp"
#include <stdint.h>
#include <string>
#include <vector>
#include <memory>

enum class CommandType
{
    UNKNOWN,
    ECHO,
    G_CODE,
    GET_POS,
    START,
    STOP,
    RESET,
    STEP,
    OK,
    ERR,
    SET_SPD_MULT,
    ZERO,
    GET_ACT_ID,
    RUN_SPINDLE,
    STOP_SPINDLE,
    DISP_TEST,
    GET_BUFF
};

class Command
{
public:
    explicit Command(CommandType type);
    explicit Command(CommandType type, std::vector<Argument> arguments);
    ~Command() = default;
    CommandType Type;
    std::vector<Argument> Arguments;
    std::string GetString();
    static std::unique_ptr<Command> Parse(std::string &str);
private:
    static CommandType StringToCommandType(std::string &str);
    static std::string CommandTypeToString(CommandType type);
};

#endif
