#ifndef __ARGUMENT_HPP__
#define __ARGUMENT_HPP__

#include <stdint.h>
#include <string>
#include <memory>

enum class ArgumentType
{
    IntNumber,
    DoubleNumber,
    NamedIntNumber,
    NamedDoubleNumber,
    StringOnly
};

class Argument
{
public:
    explicit Argument(int number);
    explicit Argument(double number);
    explicit Argument(int number, std::string &str);
    explicit Argument(double number, std::string &str);
    explicit Argument(std::string &str);
    ~Argument() = default;
    std::string GetString();
    static std::unique_ptr<Argument> Parse(std::string &str);
    ArgumentType Type;
    int IntValue = 0;
    double DoubleValue = 0.0;
    std::string StringPart;
private:
    static void FixDecimalSeparator(std::string &str);
};

#endif
