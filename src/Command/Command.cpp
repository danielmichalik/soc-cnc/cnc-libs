#include "Command.hpp"
#include <regex>
#include <sstream>
#include <iostream>
#include "Utils.hpp"

Command::Command(CommandType type)
{
    this->Type = type;
}

Command::Command(CommandType type, std::vector<Argument> arguments)
{
    this->Type = type;
    this->Arguments = arguments;
}

std::unique_ptr<Command> Command::Parse(std::string &inputString)
{
    std::string matchResult("");

    if (Utils::GetSingleMatchByRegex("\u0002(.+)\u0003", inputString, matchResult))
    {
        std::stringstream ss(matchResult);
        std::string item;
        std::vector<std::string> splittedString;
        while (std::getline(ss, item, ';'))
        {
            splittedString.push_back(item);
        }
        auto commandType = StringToCommandType(splittedString[0]);

        if (splittedString.size() == 1)
        {
            return std::move(std::make_unique<Command>(commandType));
        }

        std::vector<Argument> argumentsList;
        for (uint8_t i = 1; i < splittedString.size(); i++)
        {
            auto parsedArgument = Argument::Parse(splittedString[i]);
            argumentsList.push_back(*parsedArgument);
        }

        return std::move(std::make_unique<Command>(commandType, argumentsList));
    }

    return std::move(std::make_unique<Command>(CommandType::UNKNOWN));
}

CommandType Command::StringToCommandType(std::string &str)
{
    if (str.find("ECHO") != std::string::npos)
    {
        return CommandType::ECHO;
    }

    if (str.find("G_CODE") != std::string::npos)
    {
        return CommandType::G_CODE;
    }

    if (str.find("GET_POS") != std::string::npos)
    {
        return CommandType::GET_POS;
    }

    if (str.find("START") != std::string::npos)
    {
        return CommandType::START;
    }

    if (str.find("STOP") != std::string::npos)
    {
        return CommandType::STOP;
    }

    if (str.find("STEP") != std::string::npos)
    {
        return CommandType::STEP;
    }

    if (str.find("RESET") != std::string::npos)
    {
        return CommandType::RESET;
    }

    if (str.find("OK") != std::string::npos)
    {
        return CommandType::OK;
    }

    if (str.find("ERR") != std::string::npos)
    {
        return CommandType::ERR;
    }

    if (str.find("SET_SPD_MULT") != std::string::npos)
    {
        return CommandType::SET_SPD_MULT;
    }

    if (str.find("ZERO") != std::string::npos)
    {
        return CommandType::ZERO;
    }

    if (str.find("GET_ACT_ID") != std::string::npos)
    {
        return CommandType::GET_ACT_ID;
    }

    if (str.find("RUN_SPINDLE") != std::string::npos)
    {
        return CommandType::RUN_SPINDLE;
    }

    if (str.find("STOP_SPINDLE") != std::string::npos)
    {
        return CommandType::STOP_SPINDLE;
    }

    if (str.find("DISP_TEST") != std::string::npos)
    {
        return CommandType::DISP_TEST;
    }

    if (str.find("GET_BUFF") != std::string::npos)
    {
        return CommandType::GET_BUFF;
    }

    return CommandType::UNKNOWN;
}

std::string Command::CommandTypeToString(CommandType type)
{
    if (type == CommandType::ECHO)
    {
        return std::string("ECHO");
    }

    if (type == CommandType::G_CODE)
    {
        return std::string("G_CODE");
    }

    if (type == CommandType::GET_POS)
    {
        return std::string("GET_POS");
    }

    if (type == CommandType::START)
    {
        return std::string("START");
    }

    if (type == CommandType::STOP)
    {
        return std::string("STOP");
    }

    if (type == CommandType::STEP)
    {
        return std::string("STEP");
    }

    if (type == CommandType::RESET)
    {
        return std::string("RESET");
    }

    if (type == CommandType::OK)
    {
        return std::string("OK");
    }

    if (type == CommandType::ERR)
    {
        return std::string("ERR");
    }

    if (type == CommandType::SET_SPD_MULT)
    {
        return std::string("SET_SPD_MULT");
    }

    if (type == CommandType::ZERO)
    {
        return std::string("ZERO");
    }

    if (type == CommandType::GET_ACT_ID)
    {
        return std::string("GET_ACT_ID");
    }

    if (type == CommandType::RUN_SPINDLE)
    {
        return std::string("RUN_SPINDLE");
    }

    if (type == CommandType::STOP_SPINDLE)
    {
        return std::string("STOP_SPINDLE");
    }

    if (type == CommandType::DISP_TEST)
    {
        return std::string("DISP_TEST");
    }

    if (type == CommandType::GET_BUFF)
    {
        return std::string("GET_BUFF");
    }

    return "UNKNOWN";
}

std::string Command::GetString()
{
    std::string result("\u0002");
    result = result + CommandTypeToString(this->Type);
    if (Arguments.size() > 0)
    {
        for (auto &argument : Arguments)
        {
            result = result + ";" + argument.GetString();
        }
    }
    result = result + "\u0003";
    return result;
}
