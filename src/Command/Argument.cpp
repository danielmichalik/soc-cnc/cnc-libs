#include "Argument.hpp"
#include <regex>
#include "Utils.hpp"

Argument::Argument(int value)
{
    this->IntValue = value;
    this->Type = ArgumentType::IntNumber;
}

Argument::Argument(double value)
{
    this->DoubleValue = value;
    this->Type = ArgumentType::DoubleNumber;
}

Argument::Argument(int value, std::string &str)
{
    this->IntValue = value;
    this->StringPart = str;
    this->Type = ArgumentType::NamedIntNumber;
}

Argument::Argument(double value, std::string &str)
{
    this->DoubleValue = value;
    this->StringPart = str;
    this->Type = ArgumentType::NamedDoubleNumber;
}

Argument::Argument(std::string &str)
{
    this->StringPart = str;
    this->Type = ArgumentType::StringOnly;
}

std::unique_ptr<Argument> Argument::Parse(std::string &inputString)
{
    FixDecimalSeparator(inputString);

    std::string stringPart("");
    std::string numPart("");
    bool stringPartResult = false;
    bool doublePartResult = false;
    bool intPartResult = false;

    for(char& ch : inputString)
    {
        if(((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z')))
        {
            stringPart += ch;
        }

        if(((ch >= '0') && (ch <= '9')) || (ch == '-'))
        {
            numPart += ch;
        }

        if(ch == '.')
        {
            numPart += ch;
            doublePartResult = true;
        }
    }
    stringPartResult = (stringPart.size() > 0);
    std::string dot(".");
    intPartResult = ((numPart.size() > 0) && (!Utils::StringContains(numPart, dot)));

    if (stringPartResult && doublePartResult)
    {
        double resultDouble = std::stod(numPart);
        return std::move(std::make_unique<Argument>(resultDouble, stringPart));
    }

    if (stringPartResult && intPartResult)
    {
        int value = std::stoi(numPart);
        return std::move(std::make_unique<Argument>(value, stringPart));
    }

    if (doublePartResult)
    {
        double resultDouble = std::stod(numPart);
        return std::move(std::make_unique<Argument>(resultDouble));
    }

    if (intPartResult)
    {
        int value = std::stoi(numPart);
        return std::move(std::make_unique<Argument>(value));
    }

    return std::move(std::make_unique<Argument>(stringPart));
}

std::string Argument::GetString()
{
    if (this->Type == ArgumentType::IntNumber)
    {
        std::string result = std::to_string(this->IntValue);
        return result;
    }

    if (this->Type == ArgumentType::DoubleNumber)
    {
        std::string result = std::to_string(this->DoubleValue);
        return result;
    }

    if (this->Type == ArgumentType::NamedIntNumber)
    {
        std::string result = this->StringPart + std::to_string(this->IntValue);
        return result;
    }

    if (this->Type == ArgumentType::NamedDoubleNumber)
    {
        std::string result = this->StringPart + std::to_string(this->DoubleValue);
        return result;
    }

    return this->StringPart;
}

void Argument::FixDecimalSeparator(std::string &str)
{
    std::replace(str.begin(), str.end(), ',', '.');
}
