#ifndef __I_G_CODE_ARBITER_HPP__
#define __I_G_CODE_ARBITER_HPP__

#include <Command.hpp>

enum class ProcesResult
{
    PASS,
    ERROR,
    UNSUPPORTED
};

class IGCodeArbiter
{
public:
    virtual ProcesResult ProcessGcode(Command *command) = 0;
    virtual ~IGCodeArbiter() = default;
};
#endif
