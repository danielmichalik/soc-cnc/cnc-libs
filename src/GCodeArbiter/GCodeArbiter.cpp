#include "GCodeArbiter.hpp"
#include <Utils.hpp>
#include <exception>
#include <string>
#include <list>
#include <algorithm>

GCodeArbiter::GCodeArbiter(std::unique_ptr<IMovementControl> movementControl, std::unique_ptr<IToolControl> toolControl) :
    _movementControl{ std::move(movementControl) }, _toolControl{ std::move(toolControl) }
{
    absoluteActive = true;
}

ProcesResult GCodeArbiter::ProcessGcode(Command *command)
{
    if (command->Type != CommandType::G_CODE)
    {
        throw std::logic_error("Command type isn't G_CODE");
    }

    std::string gcode = command->Arguments[0].GetString();
    if ((Utils::StringContains(gcode, std::string("G0"))) ||
        (Utils::StringContains(gcode, std::string("G1"))))
    {
        MoveLinear(command->Arguments);
    }
    else if (Utils::StringContains(gcode, std::string("G90")))
    {
        absoluteActive = true;
    }
    else if (Utils::StringContains(gcode, std::string("G91")))
    {
        absoluteActive = false;
    }
    else if (Utils::StringContains(gcode, std::string("M3")))
    {
        if(command->Arguments.size() == 1)
        {
            _toolControl->StartSpindle(2000);
        }
        else
        {
            auto speedArgument = command->Arguments[1];
            if(!Utils::StringContains(speedArgument.StringPart, std::string("S")))
            {
                return ProcesResult::ERROR;
            }
            _toolControl->StartSpindle(speedArgument.IntValue);
        }
    }
    else if (Utils::StringContains(gcode, std::string("M5")))
    {
        _toolControl->StopSpindle();
    }
    else if (IsGcodeSupported(gcode))
    {
        // only for support confirm
    }
    else
    {
        return ProcesResult::UNSUPPORTED;
    }

    return ProcesResult::PASS;
}

bool GCodeArbiter::IsGcodeSupported(std::string code)
{
    std::list<std::string> supportedGcodes = { "G0", "G1", "G17", "G21", "G90", "G91", "M3", "M5", "M6", "M30" };
    return (std::find(supportedGcodes.begin(), supportedGcodes.end(), code) != supportedGcodes.end());
}

void GCodeArbiter::MoveLinear(std::vector<Argument> arguments)
{
    int32_t xMicrometers = 0;
    int32_t yMicrometers = 0;
    int32_t zMicrometers = 0;
    int32_t feedRate = 0;

    std::string xStepsName("X");
    std::string yStepsName("Y");
    std::string zStepsName("Z");
    std::string feedRateName("Z");

    for (auto &arg : arguments)
    {
        if (arg.StringPart.compare(xStepsName) == 0)
        {
            xMicrometers = GetArgumentValue(arg) * 1000;
        }
        else if (arg.StringPart.compare(yStepsName) == 0)
        {
            yMicrometers = GetArgumentValue(arg) * 1000;
        }
        else if (arg.StringPart.compare(zStepsName) == 0)
        {
            zMicrometers = GetArgumentValue(arg) * 1000;
        }
        else if (arg.StringPart.compare(feedRateName) == 0)
        {
            zMicrometers = GetArgumentValue(arg) * 1000;
        }
    }

    if (absoluteActive)
    {
        _movementControl->MoveAbsolute(
            xMicrometers, 
            yMicrometers,
            zMicrometers,
            feedRate);
    }
    else
    {
        _movementControl->MoveRelative(
            xMicrometers,
            yMicrometers,
            zMicrometers,
            feedRate);
    }
}

double GCodeArbiter::GetArgumentValue(Argument argument)
{
    if (argument.Type == ArgumentType::NamedDoubleNumber)
    {
        return argument.DoubleValue;
    }
    else
    {
        return (double)argument.IntValue;
    }
}