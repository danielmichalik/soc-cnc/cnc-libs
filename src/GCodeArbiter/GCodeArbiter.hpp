#ifndef __G_CODE_ARBITER_HPP__
#define __G_CODE_ARBITER_HPP__

#include <memory>
#include "IGCodeArbiter.hpp"
#include "IToolControl.hpp"
#include <MovementControl.hpp>

class GCodeArbiter : public IGCodeArbiter
{
public:
    explicit GCodeArbiter(std::unique_ptr<IMovementControl> movementControl, std::unique_ptr<IToolControl> toolControl);
    ProcesResult ProcessGcode(Command *command) override;
    bool IsGcodeSupported(std::string code);
private:
    void MoveLinear(std::vector<Argument> arguments);
    double GetArgumentValue(Argument argument);
    std::unique_ptr<IMovementControl> _movementControl;
    std::unique_ptr<IToolControl> _toolControl;
    bool absoluteActive;
};

#endif