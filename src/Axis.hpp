#ifndef __AXIS_HPP__
#define __AXIS_HPP__

enum AxisSelector
{
    X,
    Y,
    Z,
    ALL
};

#endif