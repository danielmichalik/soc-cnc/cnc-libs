#include <MemoryAccess.hpp>
#include <stdexcept>
#include "CppUTest/TestHarness.h"

uint32_t testData[4] = { 11, 12, 13, 14 };

TEST_GROUP(MemoryAccessTests)
{
};

TEST(MemoryAccessTests, ReadingDataIsCorrect)
{
    auto memoryAccess = MemoryAccess((uint32_t*)&testData[0], 16);
    CHECK_EQUAL(12, memoryAccess.ReadRegister(4));
}

TEST(MemoryAccessTests, WritingDataIsCorrect)
{
    auto memoryAccess = MemoryAccess((uint32_t*)&testData[0], 16);
    memoryAccess.WriteRegister(8, 55);
    CHECK_EQUAL(55, testData[2]);
}

TEST(MemoryAccessTests, ReadingOutOfRangeThrows)
{
    auto memoryAccess = MemoryAccess((uint32_t*)&testData[0], 16);
    CHECK_THROWS(std::logic_error, memoryAccess.ReadRegister(32));
}

TEST(MemoryAccessTests, WritingOutOfRangeThrows)
{
    auto memoryAccess = MemoryAccess((uint32_t*)&testData[0], 20);
    CHECK_THROWS(std::logic_error, memoryAccess.WriteRegister(32, 0));
}