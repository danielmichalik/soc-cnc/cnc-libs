#include <StepperMotor.hpp>
#include <MemoryAccess.hpp>
#include <stdexcept>
#include "CppUTest/TestHarness.h"

uint32_t motorRegs[7] = { 123, 234, 345, 0, 0, 0, 0 };
auto memoryAccess = std::make_unique<MemoryAccess>((uint32_t*)&motorRegs[0], 28);
auto stepperMotor = StepperMotor(std::move(memoryAccess));

TEST_GROUP(StepperMotorTests)
{
};

TEST(StepperMotorTests, EnableSetCorretBit)
{
    stepperMotor.Enable();
    CHECK_EQUAL(1, motorRegs[5]);
}

TEST(StepperMotorTests, DisableResetCorrectBit)
{
    motorRegs[5] = 1;
    stepperMotor.Disable();
    CHECK_EQUAL(0, motorRegs[5]);
}

TEST(StepperMotorTests, SetCalibModeWorks)
{
    stepperMotor.SetMode(StepperMode::CALIB);
    CHECK_EQUAL(2, motorRegs[4]);
}

TEST(StepperMotorTests, SetSpeedModeWorks)
{
    stepperMotor.SetMode(StepperMode::SPEED_CONTROL);
    CHECK_EQUAL(1, motorRegs[4]);
}

TEST(StepperMotorTests, SetPositionModeWorks)
{
    motorRegs[4] = 2;
    stepperMotor.SetMode(StepperMode::POSITION_CONTROL);
    CHECK_EQUAL(0, motorRegs[4]);
}

TEST(StepperMotorTests, GetModeWorks)
{
    motorRegs[4] = 2;
    CHECK_EQUAL(StepperMode::CALIB, stepperMotor.GetMode());
}