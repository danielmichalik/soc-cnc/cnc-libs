#include <MovementControl.hpp>
#include "StepperMotorMock.hpp"
#include <stdexcept>
#include "CppUTest/TestHarness.h"

std::unique_ptr<StepperMotorMock> stepperXMock;
std::unique_ptr<StepperMotorMock> stepperYMock;
std::unique_ptr<StepperMotorMock> stepperZMock;
StepperMotorMock * stepperMotorXMockObj;
StepperMotorMock * stepperMotorYMockObj;
StepperMotorMock * stepperMotorZMockObj;
std::unique_ptr<MovementControl> movementControl;

TEST_GROUP(MovementControlTests)
{
    void setup()
    {
        stepperXMock = std::make_unique<StepperMotorMock>();
        stepperYMock = std::make_unique<StepperMotorMock>();
        stepperZMock = std::make_unique<StepperMotorMock>();
        stepperMotorXMockObj = stepperXMock.get();
        stepperMotorYMockObj = stepperYMock.get();
        stepperMotorZMockObj = stepperZMock.get();
        movementControl = std::make_unique<MovementControl>(
            std::move(stepperXMock),
            std::move(stepperYMock),
            std::move(stepperZMock));
    }

    void teardown()
    {
        movementControl.reset();
    }
};

TEST(MovementControlTests, MoveAbsolutWorksX)
{
    movementControl->MoveAbsolute(1250, 0, 0, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorXMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorXMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorXMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}

TEST(MovementControlTests, MoveAbsolutWorksY)
{
    movementControl->MoveAbsolute(0, 1250, 0, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorYMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorYMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorYMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}

TEST(MovementControlTests, MoveAbsolutWorksZ)
{
    movementControl->MoveAbsolute(0, 0, 1250, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorZMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorZMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorZMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}

TEST(MovementControlTests, MoveRelativeWorksX)
{
    movementControl->MoveRelative(1250, 0, 0, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorXMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorXMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorXMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}

TEST(MovementControlTests, MoveRelativWorksY)
{
    movementControl->MoveRelative(0, 1250, 0, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorYMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorYMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorYMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}

TEST(MovementControlTests, MoveRelativWorksZ)
{
    movementControl->MoveRelative(0, 0, 1250, 1250 / 2);
    CHECK_EQUAL(800, stepperMotorZMockObj->RequiredPosition);
    CHECK_EQUAL(250, stepperMotorZMockObj->SpeedPeriod);
    CHECK_TRUE(StepperMode::POSITION_CONTROL == stepperMotorZMockObj->Mode);
    CHECK_EQUAL(1, stepperMotorXMockObj->GenerateSyncPulseCallCount);
}
