#include <Command.hpp>
#include "CppUTest/TestHarness.h"

TEST_GROUP(CommandGroup)
{
};

TEST(CommandGroup, Parse_EchoCommandWorks)
{
    std::string testString = "\u0002ECHO\u0003";
    auto command = Command::Parse(testString);
    CHECK_TRUE(CommandType::ECHO == command->Type);
}

TEST(CommandGroup, Parse_GcodeCommandWorks)
{
    std::string testString = "\u0002G_CODE;X25,500;Y25\u0003";
    auto command = Command::Parse(testString);
    CHECK_TRUE(CommandType::G_CODE == command->Type);
    CHECK_EQUAL(2, command->Arguments.size());
}

TEST(CommandGroup, GetString_WorksForEcho)
{
    std::string reference("\u0002ECHO\u0003");
    auto command = Command(CommandType::ECHO);
    CHECK_EQUAL(reference, command.GetString());
}

TEST(CommandGroup, GetString_WorksForGetPos)
{
    std::string reference("\u0002GET_POS;2;5;8\u0003");
    std::vector<Argument> arguments;
    arguments.push_back(Argument(2));
    arguments.push_back(Argument(5));
    arguments.push_back(Argument(8));
    auto command = Command(CommandType::GET_POS, arguments);
    CHECK_EQUAL(reference, command.GetString());
}