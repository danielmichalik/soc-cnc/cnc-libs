#include <CommandBuffer.hpp>
#include "CppUTest/TestHarness.h"

std::unique_ptr<CommandBuffer> commandBuffer;

TEST_GROUP(CommandBufferGroup)
{
    void setup()
    {
        commandBuffer = std::make_unique<CommandBuffer>(4);
    }

    void teardown()
    {
        commandBuffer.reset();
    }
};

TEST(CommandBufferGroup, BufferIsEmptyAfterCreation)
{
    CHECK_TRUE(commandBuffer->IsEmpty());
}

TEST(CommandBufferGroup, BufferIsNotEmptyAfterPush)
{
    commandBuffer->Push(Command(CommandType::ECHO));
    CHECK_FALSE(commandBuffer->IsEmpty());
}

TEST(CommandBufferGroup, FrontThrowsWhenBufferIsEmpty)
{
    CHECK_THROWS(std::logic_error, commandBuffer->Pop());
}

TEST(CommandBufferGroup, BufferIsEmptyAfterPushAndPop)
{
    commandBuffer->Push(Command(CommandType::ECHO));
    commandBuffer->Pop();
    CHECK_TRUE(commandBuffer->IsEmpty());
}

TEST(CommandBufferGroup, BufferPushAndPopWorks)
{
    commandBuffer->Push(Command(CommandType::ECHO));
    auto result = commandBuffer->Pop();
    CHECK_TRUE(CommandType::ECHO == result.Type);
}

TEST(CommandBufferGroup, BufferPushAndPopSequenceWorks)
{
    commandBuffer->Push(Command(CommandType::ECHO));
    commandBuffer->Push(Command(CommandType::START));
    commandBuffer->Push(Command(CommandType::STOP));
    CHECK_TRUE(CommandType::ECHO == commandBuffer->Pop().Type);
    CHECK_TRUE(CommandType::START == commandBuffer->Pop().Type);
    CHECK_TRUE(CommandType::STOP == commandBuffer->Pop().Type);
}

TEST(CommandBufferGroup, BufferPushAndPopSequcccnceWorks)
{
    for (int i = 0; i < 4; i++)
    {
        commandBuffer->Push(Command(CommandType::ECHO));
    }
    CHECK_THROWS(std::logic_error, commandBuffer->Push(Command(CommandType::ECHO)));
}

TEST(CommandBufferGroup, BufferIsFullAfterReachCapacity)
{
    for (int i = 0; i < 4; i++)
    {
        commandBuffer->Push(Command(CommandType::ECHO));
    }
    CHECK_TRUE(commandBuffer->IsFull());
}

TEST(CommandBufferGroup, BufferIsntFullAfterCreate)
{
    CHECK_FALSE(commandBuffer->IsFull());
}