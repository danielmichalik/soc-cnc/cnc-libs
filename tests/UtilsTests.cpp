#include <Utils.hpp>
#include "CppUTest/TestHarness.h"

TEST_GROUP(UtilsGroup)
{
};

TEST(UtilsGroup, StringContainsWorksForMatch)
{
    std::string testString("abcphrasecba");
    std::string phrase("phrase");
    CHECK_TRUE(Utils::StringContains(testString, phrase));
}

TEST(UtilsGroup, StringContainsWorksForNotMatch)
{
    std::string testString("abcphrasecba");
    std::string phrase("zzzzzz");
    CHECK_FALSE(Utils::StringContains(testString, phrase));
}

TEST(UtilsGroup, RegexWorksWithoutGroupsInExpression)
{
    std::string testString("ddd");
    std::vector<std::string> result;
    Utils::GetGroupsByRegex("ddd", testString, result);
    CHECK_EQUAL(1, result.size());
}

TEST(UtilsGroup, RegexWorksWithOneGroupInExpression)
{
    std::string testString("123abc");
    std::string reference("3a");
    std::vector<std::string> result;
    Utils::GetGroupsByRegex("2(..)b", testString, result);
    CHECK_EQUAL(2, result.size());
    CHECK_TRUE(reference == result[1]);
}

TEST(UtilsGroup, RegexWorksWithMultipleGroupsInExpression)
{
    std::string testString("abcdefgh");
    std::string reference1("bc");
    std::string reference2("fg");
    std::vector<std::string> result;
    Utils::GetGroupsByRegex("a(.+)de(.+)h", testString, result);
    CHECK_EQUAL(3, result.size());
    CHECK_TRUE(reference1 == result[1]);
    CHECK_TRUE(reference2 == result[2]);
}