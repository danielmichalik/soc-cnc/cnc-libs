#include <Argument.hpp>
#include "CppUTest/TestHarness.h"

TEST_GROUP(Argument)
{
};

TEST(Argument, Constructor_ForIntWorks)
{
    auto argument = Argument((int)25);
    CHECK_EQUAL(25, argument.IntValue);
    CHECK_TRUE(ArgumentType::IntNumber == argument.Type);
}

TEST(Argument, Constructor_ForDoubleWorks)
{
    auto argument = Argument((double)12.375);
    CHECK_EQUAL(12.375, argument.DoubleValue);
    CHECK_TRUE(ArgumentType::DoubleNumber == argument.Type);
}

TEST(Argument, Constructor_ForNamedIntWorks)
{
    std::string str = "X";
    auto argument = Argument((int)25, str);
    CHECK_EQUAL(25, argument.IntValue);
    CHECK_EQUAL(str, argument.StringPart);
    CHECK_TRUE(ArgumentType::NamedIntNumber == argument.Type);
}

TEST(Argument, Constructor_ForNamedDoubleWorks)
{
    std::string str = "Y";
    auto argument = Argument((double)12.375, str);
    CHECK_EQUAL(12.375, argument.DoubleValue);
    CHECK_EQUAL(str, argument.StringPart);
    CHECK_TRUE(ArgumentType::NamedDoubleNumber == argument.Type);
}

TEST(Argument, Constructor_ForStringWorks)
{
    std::string str = "TEXT_ONLY";
    auto argument = Argument(str);
    CHECK_EQUAL(str, argument.StringPart);
}

TEST(Argument, Parse_ForIntWorks)
{
    std::string testString = "25";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(25, argument->IntValue);
    CHECK_TRUE(ArgumentType::IntNumber == argument->Type);
}

TEST(Argument, Parse_ForDoubleWorks)
{
    std::string testString = "12.375";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(12.375, argument->DoubleValue);
    CHECK_TRUE(ArgumentType::DoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForDoubleWorksWithComma)
{
    std::string testString = "12,375";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(12.375, argument->DoubleValue);
    CHECK_TRUE(ArgumentType::DoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedIntWorks)
{
    std::string testString = "Y25";
    std::string str = "Y";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(25, argument->IntValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedIntNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedDoubleWorks)
{
    std::string testString = "X12.375";
    std::string str = "X";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(12.375, argument->DoubleValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedDoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedDoubleWorksWithComma)
{
    std::string testString = "X12,375";
    std::string str = "X";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(12.375, argument->DoubleValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedDoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForStringWorks)
{
    std::string str = "thisIsOnlyString";
    auto argument = Argument::Parse(str);
    CHECK_EQUAL(str, argument->StringPart);
}

TEST(Argument, Parse_ForIntWorksNegative)
{
    std::string testString = "-25";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-25, argument->IntValue);
    CHECK_TRUE(ArgumentType::IntNumber == argument->Type);
}

TEST(Argument, Parse_ForDoubleWorksNegative)
{
    std::string testString = "-12.375";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-12.375, argument->DoubleValue);
    CHECK_TRUE(ArgumentType::DoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForDoubleWorksWithCommaNegative)
{
    std::string testString = "-12,375";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-12.375, argument->DoubleValue);
    CHECK_TRUE(ArgumentType::DoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedIntWorksNegative)
{
    std::string testString = "Y-25";
    std::string str = "Y";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-25, argument->IntValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedIntNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedDoubleWorksNegative)
{
    std::string testString = "X-12.375";
    std::string str = "X";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-12.375, argument->DoubleValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedDoubleNumber == argument->Type);
}

TEST(Argument, Parse_ForNamedDoubleWorksWithCommaNegative)
{
    std::string testString = "X-12,375";
    std::string str = "X";
    auto argument = Argument::Parse(testString);
    CHECK_EQUAL(-12.375, argument->DoubleValue);
    CHECK_EQUAL(str, argument->StringPart);
    CHECK_TRUE(ArgumentType::NamedDoubleNumber == argument->Type);
}

TEST(Argument, GetString_WorksWithInt)
{
    std::string reference("12");
    auto argument = Argument(12);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithDouble)
{
    std::string reference("12.120000");
    auto argument = Argument(12.12);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithNamedInt)
{
    std::string reference("X12");
    std::string stringPart("X");
    auto argument = Argument(12, stringPart);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithNamedDouble)
{
    std::string reference("Y12.990000");
    std::string stringPart("Y");
    auto argument = Argument(12.99, stringPart);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithIntNegative)
{
    std::string reference("-12");
    auto argument = Argument(-12);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithDoubleNegative)
{
    std::string reference("-12.120000");
    auto argument = Argument(-12.12);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithNamedIntNegative)
{
    std::string reference("X-12");
    std::string stringPart("X");
    auto argument = Argument(-12, stringPart);
    CHECK_EQUAL(reference, argument.GetString());
}

TEST(Argument, GetString_WorksWithNamedDoubleNegative)
{
    std::string reference("Y-12.990000");
    std::string stringPart("Y");
    auto argument = Argument(-12.99, stringPart);
    CHECK_EQUAL(reference, argument.GetString());
}