#include "GCodeArbiter.hpp"
#include "MovementControlMock.hpp"
#include "ToolControlMock.hpp"
#include "CppUTest/TestHarness.h"

std::unique_ptr<IMovementControl> movementControlMock;
std::unique_ptr<IToolControl> toolControlMock;
std::unique_ptr<GCodeArbiter> gCodeArgiter;

TEST_GROUP(GCodeArbiter)
{
    void setup()
    {
        movementControlMock = std::make_unique<MovementControlMock>();
        toolControlMock = std::make_unique<ToolControlMock>();
        gCodeArgiter = std::make_unique<GCodeArbiter>(
            std::move(movementControlMock),
            std::move(toolControlMock));
    }

    void teardown()
    {
        gCodeArgiter.reset();
    }
};

TEST(GCodeArbiter, ArbiterSupportG0)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G0"));
}

TEST(GCodeArbiter, ArbiterSupportG1)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G1"));
}

TEST(GCodeArbiter, ArbiterSupportG17)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G17"));
}

TEST(GCodeArbiter, ArbiterSupportG21)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G21"));
}

TEST(GCodeArbiter, ArbiterSupportG90)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G90"));
}

TEST(GCodeArbiter, ArbiterSupportG91)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("G91"));
}

TEST(GCodeArbiter, ArbiterSupportM3)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("M3"));
}

TEST(GCodeArbiter, ArbiterSupportM5)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("M5"));
}

TEST(GCodeArbiter, ArbiterSupportM6)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("M6"));
}

TEST(GCodeArbiter, ArbiterSupportM30)
{
    CHECK_TRUE(gCodeArgiter->IsGcodeSupported("M30"));
}

TEST(GCodeArbiter, ArbiterNotSupportG2)
{
    CHECK_FALSE(gCodeArgiter->IsGcodeSupported("G2"));
}

TEST(GCodeArbiter, ArbiterNotSupportG3)
{
    CHECK_FALSE(gCodeArgiter->IsGcodeSupported("G3"));
}

TEST(GCodeArbiter, ArbiterNotSupportG20)
{
    CHECK_FALSE(gCodeArgiter->IsGcodeSupported("G20"));
}