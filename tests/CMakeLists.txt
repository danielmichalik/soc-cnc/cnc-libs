add_executable(
    tests
    tests.cpp
    MemoryAccessTests.cpp
    StepperMotorTests.cpp
    MovementControlTests.cpp
    ArgumentTests.cpp
    CommandTests.cpp
    CommandBufferTests.cpp
    UtilsTests.cpp
    GCodeArbiterTests.cpp
    ToolControlTests.cpp
    PidTests.cpp)

target_link_libraries(tests PUBLIC src CppUTest CppUTestExt)

target_include_directories(tests PUBLIC Mocks)

add_test(
    NAME tests
    COMMAND tests
)