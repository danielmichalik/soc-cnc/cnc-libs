#include "CppUTest/TestHarness.h"
#include "ToolControl.hpp"
#include "TimerControlMock.hpp"
#include "PidMock.hpp"

TimerControlMock * timerControlMockObj;
PidMock * pidMockObj;
std::unique_ptr<ToolControl> toolControl;

TEST_GROUP(ToolControlTests)
{
    void setup()
    {
        auto timerControlMock = std::make_unique<TimerControlMock>();
        timerControlMockObj = timerControlMock.get();
        auto pidMock = std::make_unique<PidMock>();
        pidMockObj = pidMock.get();
        toolControl = std::make_unique<ToolControl>(std::move(timerControlMock), std::move(pidMock));
    }

    void teardown()
    {
        toolControl.reset();
    }
};

TEST(ToolControlTests, Constructor_ModeIsSpindleAfterCreation)
{
    CHECK_TRUE(ToolControlMode::Spindle == toolControl->GetMode());
}

TEST(ToolControlTests, SetMode_SetDispenserWorks)
{
    toolControl->SetMode(ToolControlMode::Dispenser);
    CHECK_TRUE(ToolControlMode::Dispenser == toolControl->GetMode());
}

TEST(ToolControlTests, SetMode_ThrowExceptionWhenTrayChangeModeDuringSpindleRunning)
{
    toolControl->StartSpindle(2000);
    CHECK_THROWS(std::logic_error, toolControl->SetMode(Dispenser));
}

TEST(ToolControlTests, StartSpindle_SetRightValuesToTimer)
{
    toolControl->StartSpindle(10200);
    CHECK_EQUAL(50, timerControlMockObj->Frequency);
    CHECK_EQUAL(7.5, timerControlMockObj->Duty);
    CHECK_EQUAL(1, timerControlMockObj->StartPwmCallCount);
}

TEST(ToolControlTests, StopSpindle_SetRightValuesToTimer)
{
    toolControl->StopSpindle();
    CHECK_EQUAL(50, timerControlMockObj->Frequency);
    CHECK_EQUAL(5, timerControlMockObj->Duty);
}

TEST(ToolControlTests, Dispense_SetRightValuesToTimer)
{
    toolControl->Dispense(150);
    CHECK_EQUAL(150, timerControlMockObj->LengthOfPulse);
    CHECK_EQUAL(1, timerControlMockObj->GeneratePulseCallCount);
}