#include <Pid.hpp>
#include <stdexcept>
#include "CppUTest/TestHarness.h"

TEST_GROUP(PidTests)
{
};

TEST(PidTests, PidWorksWithP)
{
    auto pidController = Pid(10, 0, 0, 1, 0, 1000);
    auto result = pidController.Calculate(20, 0);
    CHECK_EQUAL(200, result);
}