#ifndef __TIMER_CONTROL_MOCK_HPP__
#define __TIMER_CONTROL_MOCK_HPP__

#include <stdint.h>
#include "ITimerControl.hpp"

class TimerControlMock : public ITimerControl
{
public:
    uint16_t StartPwmCallCount = 0;
    uint16_t Frequency = 0;
    double Duty = 0;
    void StartPwm(uint16_t frequency, double dutyPercent)
    {
        StartPwmCallCount++;
        Frequency = frequency;
        Duty = dutyPercent;
    }

    int GeneratePulseCallCount = 0;
    uint16_t LengthOfPulse = 0;
    void GeneratePulse(uint16_t lengthMiliseconds)
    {
        GeneratePulseCallCount++;
        LengthOfPulse = lengthMiliseconds;
    }

    int GetPeriodNsCallCount = 0;
    uint32_t PeriodNanoseconds = 1000.0;
    uint32_t GetPeriodNs()
    {
        GetPeriodNsCallCount++;
        return PeriodNanoseconds;
    }

    int StopCallCount = 0;
    void Stop()
    {
        StopCallCount++;
    }
};

#endif