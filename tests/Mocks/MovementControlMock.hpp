#ifndef __MOVEMENT_CONTROL_MOCK_HPP__
#define __MOVEMENT_CONTROL_MOCK_HPP__

#include <stdint.h>
#include "IMovementControl.hpp"

class MovementControlMock : public IMovementControl
{
public:
    void MoveAbsolute(uint32_t x, uint32_t y, uint32_t z, uint32_t speed) override
    {
        (void)x;
        (void)y;
        (void)z;
        (void)speed;
    }

    void MoveRelative(int32_t dx, int32_t dy, int32_t dz, uint32_t speed) override
    {
        (void)dx;
        (void)dy;
        (void)dz;
        (void)speed;
    }

    void SetHomePostion(AxisSelector axis) override
    {
        (void)axis;
    }
};

#endif