#ifndef __TOOL_CONTROL_MOCK_HPP__
#define __TOOL_CONTROL_MOCK_HPP__

class ToolControlMock : public IToolControl
{
public:
    ToolControlMode Mode = ToolControlMode::Spindle;
    void SetMode(ToolControlMode mode)
    {
        Mode = mode;
    }

    ToolControlMode GetMode()
    {
        return Mode;
    }

    void StartSpindle(uint16_t rpm)
    {
        (void)rpm;
    }

    void StopSpindle()
    {}

    void Dispense(uint16_t timeMiliseconds)
    {
        (void)timeMiliseconds;
    }
};

#endif