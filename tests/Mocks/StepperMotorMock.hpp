#ifndef __I_STEPPER_MOTOR_MOCK_HPP__
#define __I_STEPPER_MOTOR_MOCK_HPP__

#include "IStepperMotor.hpp"

class StepperMotorMock : public IStepperMotor
{
public:
    bool Enabled = false;
    void Enable() override
    {
        Enabled = true;
    }
    void Disable() override
    {
        Enabled = false;
    }

    StepperMode Mode = StepperMode::POSITION_CONTROL;
    void SetMode(StepperMode mode) override
    {
        Mode = mode;
    }
    StepperMode GetMode() override
    {
        return Mode;
    }

    uint32_t RequiredPosition = 0;
    uint32_t ActualPosition = 0;
    uint32_t GetRequiredPosition() override
    {
        return RequiredPosition;
    }
    uint32_t GetActualPosition() override
    {
        return ActualPosition;
    }
    void SetPosition(uint32_t position) override
    {
        RequiredPosition = position;
    }

    uint32_t SpeedPeriod = 0;
    uint32_t GetSpeedPeriod() override
    {
        return SpeedPeriod;
    }
    void SetSpeedPeriod(uint32_t period) override
    {
        SpeedPeriod = period;
    }

    uint32_t GenerateSyncPulseCallCount = 0;
    void GenerateSyncPulse() override
    {
        ActualPosition = RequiredPosition;
        GenerateSyncPulseCallCount++;
    }
    void SyncOn() override
    {}
    void SyncOff() override
    {}

    void ResetCounter() override
    {
        ActualPosition = 0;
    }
};

#endif