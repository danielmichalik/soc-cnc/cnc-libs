#ifndef __I_PID_MOCK_HPP__
#define __I_PID_MOCK_HPP__

#include "IPid.hpp"

class PidMock : public IPid
{
public:
    double SetValue;
    double ActualValue;
    double Calculate(double setValue, double actualValue) override
    {
        SetValue = setValue;
        ActualValue = actualValue;
        return 1;
    }
};

#endif