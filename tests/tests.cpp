#include <iostream>
#include <sstream>
#include <exception>
#include <type_traits>
#include <exception>
#include "CppUTest/CommandLineTestRunner.h"

int main(int argc, char **argv)
{
    auto returnCode = CommandLineTestRunner::RunAllTests(argc, argv);
    return returnCode;
}
